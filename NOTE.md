# Annotations
### @Subselect
The @Subselect annotation is used to specify an immutable and read-only entity using a custom SQL SELECT statement.

# Predefined fetch type
### @ * toOne
FetchType.EAGER
### @ * toMany
FetchType.LAZY