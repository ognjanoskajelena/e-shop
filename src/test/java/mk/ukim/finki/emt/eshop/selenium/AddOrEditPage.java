package mk.ukim.finki.emt.eshop.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.PageFactory;

public class AddOrEditPage extends AbstractPage {

    private WebElement name;
    private WebElement price;
    private WebElement quantity;
    private WebElement category;
    private WebElement manufacturer;
    private WebElement submit;


    public AddOrEditPage(WebDriver driver) {
        super(driver);
    }

    public static ProductsPage addProduct(HtmlUnitDriver driver, String name, String price, String quantity, String category, String manufacturer) {
        get(driver, "/products/add-form");
        AddOrEditPage addOrEditPage = PageFactory.initElements(driver, AddOrEditPage.class);

        addOrEditPage.name.sendKeys(name);
        addOrEditPage.price.sendKeys(price);
        addOrEditPage.quantity.sendKeys(quantity);
        addOrEditPage.category.click();
        addOrEditPage.category.findElement(By.xpath("//option[. = '" + category + "']")).click();
        addOrEditPage.manufacturer.click();
        addOrEditPage.manufacturer.findElement(By.xpath("//option[. = '" + manufacturer + "']")).click();

        addOrEditPage.submit.click();
        return PageFactory.initElements(driver, ProductsPage.class);
    }

    public static ProductsPage editProduct(HtmlUnitDriver driver, WebElement editBtn, String name, String price, String quantity, String category, String manufacturer) {

        editBtn.click();
        System.out.println(driver.getCurrentUrl());

        AddOrEditPage addOrEditPage = PageFactory.initElements(driver, AddOrEditPage.class);
        addOrEditPage.name.sendKeys(name);
        addOrEditPage.price.sendKeys(price);
        addOrEditPage.quantity.sendKeys(quantity);

        addOrEditPage.category.click();
        addOrEditPage.category.findElement(By.xpath("//option[. = '" + category + "']")).click();
        addOrEditPage.manufacturer.click();
        addOrEditPage.manufacturer.findElement(By.xpath("//option[. = '" + manufacturer + "']")).click();

        addOrEditPage.submit.click();
        return PageFactory.initElements(driver, ProductsPage.class);
    }
}
