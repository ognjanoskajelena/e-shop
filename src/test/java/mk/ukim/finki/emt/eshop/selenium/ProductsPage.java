package mk.ukim.finki.emt.eshop.selenium;

import lombok.Getter;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

@Getter
public class ProductsPage extends AbstractPage {

    @FindBy(css = "tr[class=productRow]")
    private List<WebElement> productRows;

    @FindBy(css = ".delete-btn")
    private List<WebElement> deleteBtns;

    @FindBy(css = ".edit-btn")
    private List<WebElement> editBtns;

    @FindBy(css = ".cart-btn")
    private List<WebElement> cartBtns;

    @FindBy(css = ".add-btn")
    private List<WebElement> addBtn;

    public ProductsPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getProductRows() {
        return productRows;
    }

    public List<WebElement> getDeleteBtns() {
        return deleteBtns;
    }

    public List<WebElement> getEditBtns() {
        return editBtns;
    }

    public List<WebElement> getCartBtns() {
        return cartBtns;
    }

    public List<WebElement> getAddBtn() {
        return addBtn;
    }

    public static ProductsPage to(WebDriver driver) {
        get(driver, "/products");
        return PageFactory.initElements(driver, ProductsPage.class);
    }

    public void assertElems(int productNo, int deleteBtnNo, int editBtnNo, int cartBtnNo, int addBtnNo) {
        Assert.assertEquals("no.of products does not match", productNo, this.getProductRows().size());
        Assert.assertEquals("no.of delete buttons not match", deleteBtnNo, this.getDeleteBtns().size());
        Assert.assertEquals("no.of edit buttons not match", editBtnNo, this.getEditBtns().size());
        Assert.assertEquals("no.of cart buttons not match", cartBtnNo, this.getCartBtns().size());
        Assert.assertEquals("no.of add buttons not match", addBtnNo, this.getAddBtn().size());
    }
}
