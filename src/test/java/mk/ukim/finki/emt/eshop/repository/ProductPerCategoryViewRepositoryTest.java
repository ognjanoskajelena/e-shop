package mk.ukim.finki.emt.eshop.repository;

import mk.ukim.finki.emt.eshop.model.view.ProductPerCategoryView;
import mk.ukim.finki.emt.eshop.repository.view.ProductPerCategoryViewRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductPerCategoryViewRepositoryTest {

    @Autowired
    private ProductPerCategoryViewRepository productPerCategoryViewRepository;

    @Test
    public void testFindAll() {
        List<ProductPerCategoryView> list =
                this.productPerCategoryViewRepository.findAll();
        Assert.assertEquals(2, list.size());
    }
}
