package mk.ukim.finki.emt.eshop.repository;

import mk.ukim.finki.emt.eshop.model.User;
import mk.ukim.finki.emt.eshop.model.enumeration.Role;
import mk.ukim.finki.emt.eshop.model.projection.UserProjection;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindAll() {
        List<User> userList = this.userRepository.findAll();
        Assert.assertEquals(1, userList.size());
    }

    @Test
    public void testFetchAll(){
        List<User> userList = this.userRepository.fetchAll();
        Assert.assertEquals(1, userList.size());
    }

    @Test
    public void testLoadAll(){
        List<User> userList = this.userRepository.loadAll();
        Assert.assertEquals(1, userList.size());
    }

    @Test
    public void testProjectUsernameAndNameAndSurname() {
        UserProjection userProjection = this.userRepository.findByRole(Role.ROLE_ADMIN);
        Assert.assertEquals("jelena.ognjanoska", userProjection.getUsername());
        Assert.assertEquals("Jelena", userProjection.getName());
        Assert.assertEquals("Ognjanoska", userProjection.getSurname());
    }

    @Test
    public void testOptimisticLock() {
        String username = "jelena.ognjanoska";

        User user1 = this.userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
        User user2 = this.userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));

        user1.setName("jelena");
        user2.setName("jelena");

        this.userRepository.save(user1);
        this.userRepository.save(user2);
    }
}
