package mk.ukim.finki.emt.eshop.selenium;

import mk.ukim.finki.emt.eshop.model.*;
import mk.ukim.finki.emt.eshop.model.enumeration.Role;
import mk.ukim.finki.emt.eshop.service.CategoryService;
import mk.ukim.finki.emt.eshop.service.ManufacturerService;
import mk.ukim.finki.emt.eshop.service.ProductService;
import mk.ukim.finki.emt.eshop.service.UserService;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class SeleniumScenarioTest {

    private HtmlUnitDriver driver;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    ManufacturerService manufacturerService;

    private static Category c1;
    private static Category c2;
    private static Manufacturer m1;
    private static Manufacturer m2;
    private static boolean isDataInitialized = false;
    private static String user = "user";
    private static String admin = "admin";
    private static User regularUser;
    private static User adminUser;

    @BeforeEach
    public void setup() {
        this.driver = new HtmlUnitDriver(true);
        this.initData();
    }

    @AfterEach
    public void destroy() {
        if (this.driver != null) {
            this.driver.close();
        }
    }

    public void initData() {
        if (!isDataInitialized) {
            isDataInitialized = true;

            c1 = categoryService.save("c1", "c1").get();
            c2 = categoryService.save("c2", "c2").get();

            m1 = manufacturerService.save("m1", "m1").get();
            m2 = manufacturerService.save("m2", "m2").get();

            regularUser = userService.register(user, user, user, user, user, Role.ROLE_USER);
            adminUser = userService.register(admin, admin, admin, admin, admin, Role.ROLE_ADMIN);
        }
    }

    @Test
    public void testScenario() {
        ProductsPage productsPage = ProductsPage.to(this.driver);
        productsPage.assertElems(0, 0, 0, 0, 0);
        LoginPage loginPage = LoginPage.openLogin(this.driver);

        productsPage = LoginPage.doLogin(this.driver, loginPage, adminUser.getUsername(), admin);
        productsPage.assertElems(0, 0, 0, 0, 1);

        productsPage = AddOrEditPage.addProduct(this.driver, "test", "100", "5", c2.getName(), m2.getName());
        productsPage.assertElems(1, 1, 1, 1, 1);

        productsPage = AddOrEditPage.addProduct(this.driver, "test1", "200", "4", c1.getName(), m2.getName());
        productsPage.assertElems(2, 2, 2, 2, 1);

        productsPage.getDeleteBtns().get(1).click();
        productsPage.assertElems(1, 1, 1, 1, 1);

        productsPage = AddOrEditPage.editProduct(this.driver, productsPage.getEditBtns().get(0), "test1", "200", "4", c1.getName(), m2.getName());
        productsPage.assertElems(1, 1, 1, 1, 1);

        loginPage = LoginPage.logout(this.driver);
        productsPage = LoginPage.doLogin(this.driver, loginPage, regularUser.getUsername(), user);
        productsPage.assertElems(1, 0, 0, 1, 0);

        productsPage.getCartBtns().get(0).click();
        Assert.assertEquals("url do not match", "http://localhost:9999/shopping-cart", this.driver.getCurrentUrl());


        ShopCartPage cartPage = ShopCartPage.init(this.driver);
        cartPage.assertElems(1);

    }

}
