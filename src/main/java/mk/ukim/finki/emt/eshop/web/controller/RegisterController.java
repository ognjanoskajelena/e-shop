package mk.ukim.finki.emt.eshop.web.controller;

import mk.ukim.finki.emt.eshop.model.enumeration.Role;
import mk.ukim.finki.emt.eshop.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private final UserService userService;

    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getRegisterPage(@RequestParam(required = false) String errorMessage, Model model) {
        if (errorMessage != null && !errorMessage.isEmpty()) {
            model.addAttribute("hasException", true);
            model.addAttribute("message", errorMessage);
        }
        model.addAttribute("bodyContent", "register");
        return "master-template";
    }

    @PostMapping
    public String register(@RequestParam String username,
                           @RequestParam String password,
                           @RequestParam String repeatedPassword,
                           @RequestParam String name,
                           @RequestParam String surname,
                           @RequestParam Role role) {

        try {
            this.userService.register(username, password, repeatedPassword, name, surname, role);
            return "redirect:/login";
        } catch (IllegalArgumentException exception) {
            return "redirect:/register?errorMessage=" + exception.getMessage();
        }
    }
}
