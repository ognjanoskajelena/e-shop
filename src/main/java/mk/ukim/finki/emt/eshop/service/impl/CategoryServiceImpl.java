package mk.ukim.finki.emt.eshop.service.impl;

import mk.ukim.finki.emt.eshop.model.Category;
import mk.ukim.finki.emt.eshop.repository.CategoryRepository;
import mk.ukim.finki.emt.eshop.service.CategoryService;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Optional<Category> save(String name, String description) {
        if (name == null || name.isEmpty()) {
            return Optional.empty();
        }
        Category category = new Category(name, description);
        this.categoryRepository.save(category);
        return Optional.of(category);
    }

    @Override
    public Category update(String name, String description) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        Category category = new Category(name, description);
        this.categoryRepository.save(category);
        return category;
    }

    @Override
    public void delete(String name) {
        if (name != null && !name.isEmpty()) {
            this.categoryRepository.deleteByName(name);
        }
    }

    @Override
    public List<Category> listCategories() {
        return this.categoryRepository.findAll();
    }

    @Override
    public List<Category> searchCategories(String searchText) {
        return this.categoryRepository.findAllByNameLike(searchText);
    }
}
