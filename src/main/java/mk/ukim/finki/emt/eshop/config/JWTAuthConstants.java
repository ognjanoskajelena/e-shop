package mk.ukim.finki.emt.eshop.config;

public class JWTAuthConstants {
    public static final long EXPIRATION_TIME = 864_000_000;
    public static final String HEADER_KEY = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String SECRET = "s3cr3tk3y";
}
