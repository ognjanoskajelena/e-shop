package mk.ukim.finki.emt.eshop.service.impl;

import mk.ukim.finki.emt.eshop.model.Category;
import mk.ukim.finki.emt.eshop.model.Manufacturer;
import mk.ukim.finki.emt.eshop.model.Product;
import mk.ukim.finki.emt.eshop.model.dto.ProductDto;
import mk.ukim.finki.emt.eshop.model.event.ProductCreatedEvent;
import mk.ukim.finki.emt.eshop.model.exception.CategoryNotFoundException;
import mk.ukim.finki.emt.eshop.model.exception.ManufacturerNotFoundException;
import mk.ukim.finki.emt.eshop.model.exception.ProductNotFoundException;
import mk.ukim.finki.emt.eshop.repository.CategoryRepository;
import mk.ukim.finki.emt.eshop.repository.ManufacturerRepository;
import mk.ukim.finki.emt.eshop.repository.ProductRepository;
import mk.ukim.finki.emt.eshop.repository.view.ProductPerManufacturerViewRepository;
import mk.ukim.finki.emt.eshop.service.ProductService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ManufacturerRepository manufacturerRepository;
    private final ProductPerManufacturerViewRepository productPerManufacturerViewRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    public ProductServiceImpl(ProductRepository productRepository,
                              CategoryRepository categoryRepository,
                              ManufacturerRepository manufacturerRepository,
                              ProductPerManufacturerViewRepository productPerManufacturerViewRepository,
                              ApplicationEventPublisher applicationEventPublisher) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.productPerManufacturerViewRepository = productPerManufacturerViewRepository;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public List<Product> findAll() {
        return this.productRepository.findAll();
    }

    @Override
    public Optional<Product> findById(Long id) {
        if (id != null) return this.productRepository.findById(id);
        return Optional.empty();
    }

    @Override
    public Optional<Product> findByName(String name) {
        if (!name.isEmpty()) return this.productRepository.findByName(name);
        return Optional.empty();
    }

    @Override
    public void deleteById(Long id) {
        Optional<Product> product = this.findById(id);
        if(product.isPresent()){
            this.productRepository.deleteById(id);
//        this.refreshMaterializedView();
            this.applicationEventPublisher.publishEvent(new ProductCreatedEvent(product.get()));
        }
        else
            throw new ProductNotFoundException(id);
    }

    @Override
    @Transactional
    public Optional<Product> save(String name, Double price, Integer quantity,
                                  Long categoryId, Long manufacturerId) {
        Category category = this.categoryRepository
                .findById(categoryId)
                .orElseThrow(() -> new CategoryNotFoundException(categoryId));

        Manufacturer manufacturer = this.manufacturerRepository
                .findById(manufacturerId)
                .orElseThrow(() -> new ManufacturerNotFoundException(manufacturerId));

        this.productRepository.deleteByName(name);
        Product product = new Product(name, price, quantity, category, manufacturer);
        this.productRepository.save(product);
//        this.refreshMaterializedView();

        this.applicationEventPublisher.publishEvent(new ProductCreatedEvent(product));
        return Optional.of(product);
    }

    @Override
    public Optional<Product> save(ProductDto productDto) {
        Category category = this.categoryRepository.findById(productDto.getCategory())
                .orElseThrow(() -> new CategoryNotFoundException(productDto.getCategory()));

        Manufacturer manufacturer = this.manufacturerRepository.findById(productDto.getManufacturer())
                .orElseThrow(() -> new ManufacturerNotFoundException(productDto.getManufacturer()));

        this.productRepository.deleteByName(productDto.getName());
        Product product = new Product(productDto.getName(), productDto.getPrice(), productDto.getQuantity(), category, manufacturer);
        this.productRepository.save(product);
//        this.refreshMaterializedView();

        this.applicationEventPublisher.publishEvent(new ProductCreatedEvent(product));
        return Optional.of(product);
    }

    @Override
    @Transactional
    public Optional<Product> edit(Long id, String name, Double price, Integer quantity, Long categoryId, Long manufacturerId) {
        Product product = this.productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));

        product.setName(name);
        product.setPrice(price);
        product.setQuantity(quantity);

        Category category = this.categoryRepository
                .findById(categoryId).orElseThrow(() -> new CategoryNotFoundException(categoryId));
        product.setCategory(category);

        Manufacturer manufacturer = this.manufacturerRepository
                .findById(manufacturerId).orElseThrow(() -> new ManufacturerNotFoundException(manufacturerId));
        product.setManufacturer(manufacturer);

        this.productRepository.save(product);
//        this.refreshMaterializedView();

        return Optional.of(product);
    }

    @Override
    public Optional<Product> edit(Long id, ProductDto productDto) {
        Product product = this.productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));

        product.setName(productDto.getName());
        product.setPrice(productDto.getPrice());
        product.setQuantity(productDto.getQuantity());

        Category category = this.categoryRepository.findById(productDto.getCategory())
                .orElseThrow(() -> new CategoryNotFoundException(productDto.getCategory()));
        product.setCategory(category);

        Manufacturer manufacturer = this.manufacturerRepository.findById(productDto.getManufacturer())
                .orElseThrow(() -> new ManufacturerNotFoundException(productDto.getManufacturer()));
        product.setManufacturer(manufacturer);

        this.productRepository.save(product);
//        this.refreshMaterializedView();

        return Optional.of(product);
    }

    @Override
    public void refreshMaterializedView() {
        this.productPerManufacturerViewRepository.refreshMaterializedView();
    }

    @Override
    public Optional<Product> save(Product product) {
        this.categoryRepository.findById(product.getCategory().getId())
                .orElseThrow(() -> new CategoryNotFoundException(product.getCategory().getId()));

        this.manufacturerRepository.findById(product.getManufacturer().getId())
                .orElseThrow(() -> new ManufacturerNotFoundException(product.getManufacturer().getId()));

        this.productRepository.save(product);
//        this.refreshMaterializedView();

        this.applicationEventPublisher.publishEvent(new ProductCreatedEvent(product));
        return Optional.of(product);
    }

//    @Override
//    public int totalProducts(Long id) {
//        return this.productRepository.totalProducts(id);
//    }
}
