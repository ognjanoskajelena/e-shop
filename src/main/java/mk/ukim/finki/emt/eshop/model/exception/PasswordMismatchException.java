package mk.ukim.finki.emt.eshop.model.exception;

public class PasswordMismatchException extends RuntimeException {
    public PasswordMismatchException(String s) {
        super(s);
    }
}
