package mk.ukim.finki.emt.eshop.model.projection;

import mk.ukim.finki.emt.eshop.model.User;

import java.time.LocalDateTime;
import java.util.List;

public interface DiscountProjection {
    LocalDateTime getValidUntil();
    List<User> getUsers();
}
