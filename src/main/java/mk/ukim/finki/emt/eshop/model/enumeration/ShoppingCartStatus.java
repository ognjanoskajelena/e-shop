package mk.ukim.finki.emt.eshop.model.enumeration;

public enum ShoppingCartStatus {
    CREATED,
    CANCELED,
    FINISHED
}