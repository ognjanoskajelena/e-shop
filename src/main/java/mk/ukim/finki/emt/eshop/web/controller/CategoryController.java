package mk.ukim.finki.emt.eshop.web.controller;

import mk.ukim.finki.emt.eshop.model.Category;
import mk.ukim.finki.emt.eshop.service.CategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = {"/category"})
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public String getCategoryPage(HttpServletRequest request, Model model) {
        List<Category> categories = this.categoryService.listCategories();
        String userAgent = request.getHeader("User-Agent");
        String ipAddress = request.getRemoteAddr();

        model.addAttribute("categories", categories);
        model.addAttribute("userAgent", userAgent);
        model.addAttribute("ipAddress", ipAddress);
        model.addAttribute("bodyContent", "categories");
        return "master-template";
    }

    @PostMapping(value = {"/add"})
    public String addCategory(@RequestParam String name,
                              @RequestParam String description,
                              Model model) {
        Optional<Category> category = this.categoryService.save(name, description);
        if (category.isEmpty()) {
            model.addAttribute("hasException", true);
            model.addAttribute("message", "Required parameters for category creation!");
        }
        return "redirect:/category";
    }
}
