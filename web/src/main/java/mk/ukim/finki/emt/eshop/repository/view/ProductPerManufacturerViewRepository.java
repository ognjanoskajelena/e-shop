package mk.ukim.finki.emt.eshop.repository.view;

import mk.ukim.finki.emt.eshop.model.view.ProductPerManufacturerView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ProductPerManufacturerViewRepository
        extends JpaRepository<ProductPerManufacturerView, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "REFRESH MATERIALIZED VIEW public.product_per_manufacturer",
            nativeQuery = true)
    void refreshMaterializedView();
}
