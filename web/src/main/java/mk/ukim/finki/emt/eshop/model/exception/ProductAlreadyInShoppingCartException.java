package mk.ukim.finki.emt.eshop.model.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
public class ProductAlreadyInShoppingCartException extends RuntimeException {

    public ProductAlreadyInShoppingCartException(Long productId, String username) {
        super(String.format("Product with id %d already exists in shopping cart for username %s!", productId, username));
    }
}
