package mk.ukim.finki.emt.eshop.model.projection;

public interface UserProjection {
    String getUsername();
    String getName();
    String getSurname();
}
