package mk.ukim.finki.emt.eshop.repository.view;

import mk.ukim.finki.emt.eshop.model.view.ProductPerCategoryView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductPerCategoryViewRepository
        extends JpaRepository<ProductPerCategoryView, Long> {

}
