package mk.ukim.finki.emt.eshop.selenium;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

public class ShopCartPage extends AbstractPage {

    @FindBy(css = "tr[class=cart-item]")
    private List<WebElement> cartRows;

    public ShopCartPage(WebDriver driver) {
        super(driver);
    }

    public static ShopCartPage init(WebDriver driver) {
        return PageFactory.initElements(driver, ShopCartPage.class);
    }

    public void assertElems(int cartItemsNo) {
        Assert.assertEquals("Item-rows mismatch", cartItemsNo, this.getCartRows().size());
    }

    public List<WebElement> getCartRows() {
        return cartRows;
    }
}
