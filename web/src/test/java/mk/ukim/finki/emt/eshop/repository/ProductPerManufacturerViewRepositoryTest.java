package mk.ukim.finki.emt.eshop.repository;

import mk.ukim.finki.emt.eshop.model.Product;
import mk.ukim.finki.emt.eshop.model.view.ProductPerManufacturerView;
import mk.ukim.finki.emt.eshop.repository.view.ProductPerManufacturerViewRepository;
import mk.ukim.finki.emt.eshop.service.CategoryService;
import mk.ukim.finki.emt.eshop.service.ManufacturerService;
import mk.ukim.finki.emt.eshop.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.parameters.P;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductPerManufacturerViewRepositoryTest {

    @Autowired
    private ProductPerManufacturerViewRepository productPerManufacturerViewRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ManufacturerService manufacturerService;

    @Autowired
    private ProductService productService;

    @Test
    public void testCreateNewProduct() {
        List<ProductPerManufacturerView> list1 =
                this.productPerManufacturerViewRepository.findAll();

        Product product = new Product();
        product.setName("TestProductEvent");
        product.setCategory(this.categoryService.listCategories().get(0));
        product.setManufacturer(this.manufacturerService.findAll().get(0));
        this.productService.save(product);

        List<ProductPerManufacturerView> list2 =
                this.productPerManufacturerViewRepository.findAll();

    }
}
