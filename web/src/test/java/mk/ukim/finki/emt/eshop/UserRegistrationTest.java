package mk.ukim.finki.emt.eshop;

import mk.ukim.finki.emt.eshop.model.enumeration.Role;
import mk.ukim.finki.emt.eshop.model.User;
import mk.ukim.finki.emt.eshop.model.exception.PasswordMismatchException;
import mk.ukim.finki.emt.eshop.model.exception.UsernameAlreadyExistsException;
import mk.ukim.finki.emt.eshop.repository.UserRepository;
import mk.ukim.finki.emt.eshop.service.UserService;
import mk.ukim.finki.emt.eshop.service.impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserRegistrationTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    private UserService userService;


    @Before
    public void init() {

        MockitoAnnotations.initMocks(this);
        User user = new User("username", "password", "name", "surname", Role.ROLE_USER);

        Mockito.when(this.userRepository.save(Mockito.any(User.class))).thenReturn(user);

        Mockito.when(this.passwordEncoder.encode(Mockito.anyString())).thenReturn("password");

        this.userService = Mockito.spy(new UserServiceImpl(this.userRepository, this.passwordEncoder));

    }

    @Test
    public void testSuccessRegister() {
        User user = this.userService.register("username", "password", "password", "name", "surname", Role.ROLE_USER);

        Mockito.verify(this.userService).register("username", "password", "password", "name", "surname", Role.ROLE_USER);

        Assert.assertNotNull("User is null", user);
        Assert.assertEquals("Names do not match", "name", user.getName());
        Assert.assertEquals("Surnames do not match", "surname", user.getSurname());
        Assert.assertEquals("Passwords do not match", "password", user.getPassword());
        Assert.assertEquals("Usernames do not match", "username", user.getUsername());
        Assert.assertEquals("Roles do not match", Role.ROLE_USER, user.getRole());
    }

    @Test
    public void testNullUsername() {
        Assert.assertThrows("IllegalArgumentException exception expected", IllegalArgumentException.class,
                () -> this.userService.register(null, "password", "password", "name", "surname", Role.ROLE_USER));
        Mockito.verify(this.userService).register(null, "password", "password", "name", "surname", Role.ROLE_USER);
    }

    @Test
    public void testEmptyUsername() {
        String username = "";
        Assert.assertThrows("IllegalArgumentException exception expected", IllegalArgumentException.class,
                () -> this.userService.register(username, "password", "password", "name", "surname", Role.ROLE_USER));
        Mockito.verify(this.userService).register(username, "password", "password", "name", "surname", Role.ROLE_USER);
    }

    @Test
    public void testNullPassword() {
        String username = "username";
        Assert.assertThrows("IllegalArgumentException exception expected", IllegalArgumentException.class,
                () -> this.userService.register(username, null, "password", "name", "surname", Role.ROLE_USER));
        Mockito.verify(this.userService).register(username, null, "password", "name", "surname", Role.ROLE_USER);
    }

    @Test
    public void testEmptyPassword() {
        String username = "username";
        String password = "";
        Assert.assertThrows("IllegalArgumentException exception expected", IllegalArgumentException.class,
                () -> this.userService.register(username, password, "password", "name", "surname", Role.ROLE_USER));
        Mockito.verify(this.userService).register(username, password, "password", "name", "surname", Role.ROLE_USER);
    }

    @Test
    public void testMismatchPassword() {
        String password = "password";
        String repeatedPassword = "other password";

        Assert.assertThrows("PasswordMismatchException exception expected", PasswordMismatchException.class,
                () -> this.userService.register("username", password, repeatedPassword, "name", "surname", Role.ROLE_USER));
        Mockito.verify(this.userService).register("username", password, repeatedPassword, "name", "surname", Role.ROLE_USER);
    }

    @Test
    public void testDuplicateUsername() {
        User user = new User("username", "password", "name", "surname", Role.ROLE_USER);

        Mockito.when(this.userRepository.findByUsername(Mockito.anyString())).thenReturn(Optional.of(user));

        Assert.assertThrows("UsernameAlreadyExistsException exception expected", UsernameAlreadyExistsException.class,
                () -> this.userService.register("username", "password", "password", "name", "surname", Role.ROLE_USER));
        Mockito.verify(this.userService).register("username", "password", "password", "name", "surname", Role.ROLE_USER);

    }
}
